var http = require('http');
var fs = require('fs');
var parser = require('csv-parser');
var qs = require('querystring');
var jwt = require('jsonwebtoken')


//mandatory headers
const stagingSecretKey = "feeb495e38c7d784b556da3b772c5e800cda509d";
const stagingClientId = "stg-paytm-merchantBusiness";
const stagingHost = "notifications-platformproducer-staging.paytm.com"
const prodSecretKey = "85f0dd14b48a63332550054d0fb5a9f2b932f316";
const prodClientId = "prod-paytm-merchantBusiness";
const prodHost = "notificationplatform-internal.paytm.com";
const bossStagHost = "pg-staging3.paytm.in"
const bossClientId = "66c02d3e-ebc0-4117-ba85-7f523ac8d424"
const bossClientKey = "WidnixnDo2780hILxdvvQXu9shJ9tIZnSsX4aEe9aKoOg5n7CtkijYqCb0ijNe7SE1qOu38JVU+gfx8G89oWvQ=="
const jwtExpirySeconds = 300

list = [];   //whole data will be stored here
delayBetweenRequests = 300; //milliseconds

function getJwtToken(){
	let buff = new Buffer(bossClientKey, 'base64');
	let text = buff.toString('ascii');
	const token = jwt.sign({ "client-id" : bossClientId ,"iat": Math.floor(Date.now()/1000)},Buffer.from(bossClientKey, 'base64') , {
		algorithm: "HS512"
	});
	console.log("Token:", token)
	return token;
}

function getMobile(i,mid) {
	console.log("inside getMobile:",mid);
	var options = {
		host : bossStagHost,
		path : "/api/v1/merchant/" + mid + "/basic/info",
		headers : {
				"content-type":"application/json",
				"x-client-token":getJwtToken(),
				"x-client-id":bossClientId
		},
		method :"GET",
	};

	
	const request = http.request(options, (res)=>{
		let str = '';
		res.on('data',(data)=>{
			console.log("RESPONSE:");
			console.log(str+data);
			var jsonObject = JSON.parse(data);

			if(typeof jsonObject === 'undefined' || typeof jsonObject.primaryMobileNumber === 'undefined')
				console.log("Mobile Number Not Found for mid:",mid);
			else
				sendMessage(i,jsonObject['primaryMobileNumber'])
		});
		res.on('end',()=>{console.log("TIMESTAMP:",Date.now());
		                  console.log("Boss MESSAGE DETAILS:");
		                  });
	});

	request.on('socket', function (socket) {
	    socket.setTimeout(5000);  
	});

	request.on('error',(err)=>{
		console.log("error:",err);
		if (err.code === "ECONNRESET") {
        	console.log("Timeout occurs for mid:",mid);
    	}
	});

	request.on('Error',(e)=>{
		console.log("ERROR:",e);
	});
	request.end();

}

function sendMessage(i,mobile){
	setTimeout(()=>{
		var template = null;
		var dynamicParameters = {};
		var mobileNumbers=[];
		mobileNumbers.push(mobile);
		console.log('Mobile:',mobile);
		var jsonObject=list[i];
		var count = null;
		for(var key in jsonObject){
			if(key=="template_type")
				template = jsonObject[key];
			else if(key=="no_of_message_variables")
				count=jsonObject[key];
			else{
				if(count==0)
					break;
				else{
					dynamicParameters[key]=jsonObject[key];
					count--;
				}
			}
		}
		var post_data={
			    "templateName": template,
			    "notificationReceiver": {
			        "encrypted": false,
			        "notificationReceiverType": "MOBILENUMBER",
			        "notificationReceiverIdentifier": mobileNumbers
			    },
			    "dynamicParams": dynamicParameters,
			    "callback": {
			        "method": "POST",
			        "url": "http://10.254.16.31:8080/v3/notify/testCallback"
			    },
			    "scheduleNotification": {
			        "scheduleTimeType": "SECS",
			        "scheduleTimeValue": 1
			    }
			}
		var options = {
			host : stagingHost,
			path : "/v3/notify/whatsapp",
			headers : {
					"content-type":"application/json",
					"secret_key":stagingSecretKey,
					"client_id":stagingClientId
			},
			method :"POST",
		};

		const request = http.request(options, (res)=>{
			let str = '';
			res.on('data',(data)=>{
				console.log("RESPONSE:");
				console.log(str+data);
			});
			res.on('end',()=>{console.log("TIMESTAMP:",Date.now());
			                  console.log("MESSAGE DETAILS:");
			                  console.log(post_data);});
		});
		request.on('Error',(e)=>{
			console.log("ERROR:",e);
		});
		request.write(JSON.stringify(post_data));
		request.end();
	}, i*delayBetweenRequests);
}

function traverseList(){
	for(let i=0; i<list.length; i++){
		var jsonObject=list[i];
		for(var key in jsonObject){
			if(key=="mid"){
				getMobile(i,jsonObject[key]);
			}
		}
	} 
}
function start(){
	const file= fs.createReadStream('sample.csv').pipe(parser())
	file.on('data',( data)=>{
		list.push(data);
	});
	file.on('end',traverseList);
}
start();
